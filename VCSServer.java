import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class VCSServer {
    private ServerSocket socket;
    private Repository repository;

    public VCSServer(int port) throws IOException
    {
        socket = new ServerSocket(port);
        repository = new Repository();
    }

    public void start() throws IOException
    {
        System.out.println("Starting serving...");

        BufferedReader reader;
        PrintWriter writer;
        Socket fromclient;

        while (true)
        {
            System.out.println("Started.");

            fromclient = socket.accept();
            System.out.println("Client is accepted");

            reader = new BufferedReader(new InputStreamReader(fromclient.getInputStream()));
            writer = new PrintWriter(fromclient.getOutputStream(), true);

            System.out.println("Messages:");
            String inputLine;
            String fullText = "";
            while ((inputLine = reader.readLine()) != null)
            {
                System.out.println(inputLine);
                fullText += inputLine;
            }

            System.out.println(fullText);

            writer.print("Your question: " + fullText);

            if (fullText.equalsIgnoreCase("exit"))
                break;

            fromclient.close();
        }
        reader.close();
        writer.close();

        System.out.println("Bye ;)");
    }


//    Остальные private
    private void init(File path) throws IOException
    {
    }

    private void commit(File[] files) throws IOException, UnsupportedOperationException
    {
    }

    private void log()
    {
    }

    private void switchActiveCommit(int index) throws IndexOutOfBoundsException
    {
    }

    private void update() throws IOException
    {
    }

    private void cloneRepository(File target) throws IOException
    {
    }

    private void copy(File[] sourceFiles, File dest) throws IOException, NullPointerException
    {
    }
}
