import java.io.File;
import java.time.Instant;

public class Commit {
    private File commitDirectory;
    private Instant createTime;
    private File[] commitFiles;

    public Commit(File commitDirectory, File[] commitFiles)
    {
        this.commitDirectory = commitDirectory;
        this.commitFiles = commitFiles;
        this.createTime = Instant.now();
    }

    public Commit(File commitDirectory, File[] commitFiles, Instant createTime)
    {
        this.commitDirectory = commitDirectory;
        this.commitFiles = commitFiles;
        this.createTime = createTime;
    }

    public Instant getCreateTime()
    {
        return createTime;
    }

    public File getCommitDirectory()
    {
        return commitDirectory;
    }

    public File[] getCommitFiles()
    {
        return commitFiles != null ? commitFiles.clone() : null;
    }
}
