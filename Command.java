import java.nio.ByteBuffer;

public class Command implements ISerializable {
    public static final byte INIT = 0;
    public static final byte COMMIT = 1;
    public static final byte LOG = 2;
    public static final byte SWITCH = 3;
    public static final byte UPDATE = 4;
    public static final byte CLONE = 5;

    private byte code;
    private byte[] data;

    public Command(byte code, byte[] data)
    {
        this.code = code;
        this.data = data;
    }

    public byte getCode()
    {
        return code;
    }

    public byte[] getData()
    {
        return data;
    }

    public byte[] serialize()
    {
        ByteBuffer x = ByteBuffer.allocate(1 + data.length);
        x.put(code);
        x.put(data);
        return x.array();
    }

    public Command deserialize(byte[] bytes)
    {
        ByteBuffer x = ByteBuffer.allocate(bytes.length);

        byte[] res = new byte[bytes.length - 1];
        x.get(res, 1, bytes.length);
        return new Command(x.get(0), res);
    }
}
