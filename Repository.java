import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.util.*;


public class Repository {
    private volatile File repoDirectory;
    private volatile File rootCommitDirectory;

    private volatile List<Commit> commitChain;
    private volatile int lastCommitIndex;
    private volatile int activeCommitIndex;

    public Repository() {}

    public Repository(File repoDirectory) throws IOException
    {
        this.repoDirectory = repoDirectory;

        rootCommitDirectory = new File(repoDirectory, "commits");
        rootCommitDirectory.mkdir();

        commitChain = new LinkedList<>();
        lastCommitIndex = -1;
        activeCommitIndex = -1;

        File[] commitDirs = rootCommitDirectory.listFiles();
        if (commitDirs != null)
        {
            for (File commitDir : commitDirs)
            {
                lastCommitIndex++;
                activeCommitIndex++;

                File newName = new File(rootCommitDirectory, String.valueOf(lastCommitIndex));
                commitDir.renameTo(newName);

                Commit commit = new Commit(commitDir, commitDir.listFiles(),
                        Instant.ofEpochMilli(commitDir.lastModified()));
                commitChain.add(commit);
            }
        }
    }

    private void deleteDirectory(File file) throws IOException {
        if (file.isDirectory())
        {
            File[] x = file.listFiles();
            if (x != null)
                for (File c : x)
                    deleteDirectory(c);
        }

        if (!file.delete())
            throw new FileNotFoundException("Failed to delete file: " + file);
    }

    private boolean createDirectory(File file)throws IOException
    {
        if (file.exists())
            deleteDirectory(file);
        return file.mkdir();
    }

    private void copy(File[] sourceFiles, File dest) throws IOException, NullPointerException
    {
        if (sourceFiles == null)
            return;

        if (dest == null)
            throw new NullPointerException();

        for (File source : sourceFiles)
        {
            Path sp = source.toPath();
            Path dp = dest.toPath();
            Path res = dp.resolve(sp.getFileName());
            Files.copy(sp, res);
        }
    }

    public void log()
    {
        int num = 1;
        for (Commit commit : commitChain)
        {
            System.out.println("Commit " + String.valueOf(num) + ".");
            System.out.println("  Create time: " + commit.getCreateTime().toString());
            num++;
        }
    }

    public void switchActiveCommit(int index) throws IndexOutOfBoundsException
    {
        if (index < 0 || index > lastCommitIndex)
            throw new IndexOutOfBoundsException();

        activeCommitIndex = index;
    }

    public void commit(File[] files) throws IOException, UnsupportedOperationException
    {
        if (lastCommitIndex != activeCommitIndex)
            throw new UnsupportedOperationException("Commits were switched.");

        lastCommitIndex++;
        activeCommitIndex++;

        File commitDir = new File(rootCommitDirectory,
                                  String.valueOf(lastCommitIndex));
        createDirectory(commitDir);
        copy(files, commitDir);

        Commit commit = new Commit(commitDir, files);
        commitChain.add(commit);
    }

    public void update() throws IOException
    {
        if (lastCommitIndex != activeCommitIndex)
            throw new UnsupportedOperationException("Commits were switched.");

        Map<String, File> latestFiles = new HashMap<>();
        for (Commit commit : commitChain)
        {
            if (commit.getCommitFiles() == null)
                continue;

            for (File file : commit.getCommitFiles())
            {
                String name = file.getName();
                File storedFile = latestFiles.getOrDefault(name, null);
                if (storedFile != null)
                {
                    if (storedFile.lastModified() < file.lastModified())
                        latestFiles.put(name, file);
                }
                else
                    latestFiles.put(name, file);
            }
        }

        File[] updatedFiles = new File[latestFiles.size()];
        int i = 0;
        for (File file : latestFiles.values())
        {
            updatedFiles[i] = file;
            i++;
        }

        commit(updatedFiles);
    }

    public void cloneRepository(File target) throws IOException
    {
        update();
        createDirectory(target);

        File targetCommitDirectory = new File(target, "commits");
        createDirectory(targetCommitDirectory);

        File firstCommitDir = new File(targetCommitDirectory, "0");
        createDirectory(firstCommitDir);

        File[] repoFiles = commitChain.get(lastCommitIndex).getCommitFiles();

        copy(repoFiles, firstCommitDir);
    }

    public boolean init(File path) throws IOException
    {
        repoDirectory = path;
        rootCommitDirectory = new File(repoDirectory, "commits");

        commitChain = new LinkedList<>();
        lastCommitIndex = -1;
        activeCommitIndex = -1;

        return createDirectory(repoDirectory) &&
                createDirectory(rootCommitDirectory);
    }
}
