import java.util.Arrays;

public class TransFile implements ISerializable {
    private String fileName;
    private String content;

    public TransFile(String fileName, String content)
    {
        this.fileName = fileName;
        this.content = content;
    }

    public String getFileName()
    {
        return fileName;
    }

    public String getContent()
    {
        return content;
    }

    public byte[] serialize()
    {
//        fileName.getBytes()
        return new byte[0];
    }

    public TransFile deserialize(byte[] bytes)
    {
//        ByteBuffer buff = ByteBuffer.allocate(bytes.length);
//        buff.put(bytes);

//        byte
//        buff.get()
        return new TransFile(Arrays.toString(bytes), Arrays.toString(bytes));
    }

}
