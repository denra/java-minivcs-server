package transfer.commands.impl;

import transfer.commands.base.NonMetaFileCommand;
import transfer.commands.base.CommandCodes;
import transfer._exceptions.UnknownCodeException;

/**
 * Created by denra on 08.06.17.
 */
public class CommitCommand extends NonMetaFileCommand {

    public CommitCommand(int fileCount) throws UnknownCodeException {
        super(CommandCodes.COMMIT, fileCount);
    }
}
