package transfer.commands.impl;

import transfer.commands.base.MetaCommand;
import transfer.commands.base.CommandCodes;
import transfer._exceptions.UnknownCodeException;

/**
 * Created by denra on 08.06.17.
 */
public class SwitchToServerCommand extends MetaCommand {

    public SwitchToServerCommand(int revisionNumber) throws UnknownCodeException {
        super(CommandCodes.SWITCH, Integer.toString(revisionNumber));
    }
}
