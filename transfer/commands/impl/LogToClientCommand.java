package transfer.commands.impl;

import transfer.commands.base.NonMetaFileCommand;
import transfer.commands.base.CommandCodes;
import transfer._exceptions.UnknownCodeException;

/**
 * Created by denra on 08.06.17.
 */
public class LogToClientCommand extends NonMetaFileCommand {

    public LogToClientCommand() throws UnknownCodeException {
        super(CommandCodes.LOG, 1);
    }
}
