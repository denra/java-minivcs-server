package transfer.commands.impl;

import transfer.commands.base.NonMetaCommand;
import transfer.commands.base.CommandCodes;
import transfer._exceptions.UnknownCodeException;

/**
 * Created by denra on 08.06.17.
 */
public class UpdateToServerCommand extends NonMetaCommand {
    public UpdateToServerCommand() throws UnknownCodeException {
        super(CommandCodes.UPDATE);
    }
}
