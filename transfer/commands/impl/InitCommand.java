package transfer.commands.impl;

import transfer.commands.base.MetaCommand;
import transfer.commands.base.CommandCodes;
import transfer._exceptions.UnknownCodeException;

/**
 * Created by denra on 08.06.17.
 */
public class InitCommand extends MetaCommand {

    public InitCommand(String repoPath) throws UnknownCodeException {
        super(CommandCodes.INIT, repoPath);
    }
}
