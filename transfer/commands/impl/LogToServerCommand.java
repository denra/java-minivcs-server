package transfer.commands.impl;

import transfer.commands.base.NonMetaCommand;
import transfer.commands.base.CommandCodes;
import transfer._exceptions.UnknownCodeException;

/**
 * Created by denra on 08.06.17.
 */
public class LogToServerCommand extends NonMetaCommand {
    public LogToServerCommand() throws UnknownCodeException {
        super(CommandCodes.LOG);
    }
}
