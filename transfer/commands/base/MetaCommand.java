package transfer.commands.base;

import transfer._bases.BaseMetaOperation;
import transfer._exceptions.UnknownCodeException;

/**
 * Created by denra on 08.06.17.
 */

public class MetaCommand extends BaseMetaOperation
{
    public MetaCommand(byte code, String meta) throws UnknownCodeException
    {
        super(code, CommandCodes.commandName, meta);
    }
}
