package transfer.commands.base;

import transfer._bases.BaseOperation;
import transfer._exceptions.UnknownCodeException;

/**
 * Created by denra on 08.06.17.
 */
public class NonMetaCommand extends BaseOperation{
    public NonMetaCommand(byte code) throws UnknownCodeException
    {
        super(code, CommandCodes.commandName);
    }

}
