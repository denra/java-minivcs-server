package transfer.commands.base;

import transfer._bases.BaseFileOperation;
import transfer._exceptions.UnknownCodeException;

/**
 * Created by denra on 08.06.17.
 */
public class NonMetaFileCommand extends BaseFileOperation {

    public NonMetaFileCommand(byte code, int fileCount) throws UnknownCodeException {
        super(code, CommandCodes.commandName, fileCount);
    }
}
