package transfer.commands.base;

/**
 * Created by denra on 08.06.17.
 */
public class CommandCodes {
    public static final byte INIT = 0;
    public static final byte COMMIT = 1;
    public static final byte LOG = 2;
    public static final byte SWITCH = 3;
    public static final byte UPDATE = 4;
    public static final byte CLONE = 5;
    
    public static final String[] commandName = new String[6];

    static {
        commandName[INIT] = "init";
        commandName[COMMIT] = "commit";
        commandName[LOG] = "log";
        commandName[SWITCH] = "switch";
        commandName[UPDATE] = "update";
        commandName[CLONE] = "clone";
    }
}
