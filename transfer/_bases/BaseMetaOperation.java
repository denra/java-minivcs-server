package transfer._bases;

import transfer._exceptions.UnknownCodeException;
import transfer._interfaces.IMetaOperation;

/**
 * Created by denra on 08.06.17.
 */
public class BaseMetaOperation extends BaseOperation implements IMetaOperation {
    private String m_meta;

    public BaseMetaOperation(byte code, String[] codeToName, String meta) throws UnknownCodeException {
        super(code, codeToName);
        m_meta = meta;
    }

    @Override
    public String getMeta() {
        return m_meta;
    }
}
