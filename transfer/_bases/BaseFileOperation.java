package transfer._bases;

import transfer._exceptions.UnknownCodeException;
import transfer._interfaces.IFileOperation;

/**
 * Created by denra on 08.06.17.
 */
public class BaseFileOperation extends BaseOperation implements IFileOperation{
    int m_fileCount;

    public BaseFileOperation(byte code, String[] codeToName, int fileCount) throws UnknownCodeException {
        super(code, codeToName);
        m_fileCount = fileCount;
    }

    public int getReceivingFileCount() {
        return m_fileCount;
    }
}
