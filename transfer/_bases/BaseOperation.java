package transfer._bases;

import transfer._exceptions.UnknownCodeException;
import transfer._interfaces.IOperation;

/**
 * Created by denra on 08.06.17.
 */
public class BaseOperation implements IOperation{
        private byte m_code;
        private String m_name;

        public BaseOperation(byte code, String[] codeToName) throws UnknownCodeException
        {
            try
            {
                m_code = code;
                m_name = codeToName[code];
            }
            catch (ArrayIndexOutOfBoundsException e)
            {
                throw new UnknownCodeException();
            }
        }

        public byte getCode() {
            return m_code;
        }

        public String getName() {
            return m_name;
        }
}

