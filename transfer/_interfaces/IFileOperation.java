package transfer._interfaces;

/**
 * Created by denra on 08.06.17.
 */
public interface IFileOperation {
    public int getReceivingFileCount();
}
