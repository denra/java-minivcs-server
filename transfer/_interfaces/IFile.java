package transfer._interfaces;

/**
 * Created by denra on 08.06.17.
 */
public interface IFile {
    public byte[] getData();

    public String getRepoName();
    public String getPathOverRepo();

    public boolean isMeta();

    public String getBeforeSendHash();
    public String getCurrentHash();
}
