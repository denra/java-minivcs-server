package transfer.notices;

import transfer._exceptions.UnknownCodeException;
import transfer._interfaces.IOperation;
import transfer.notices.base.NoticeCodes;

/**
 * Created by denra on 08.06.17.
 */
public class BaseNotice implements IOperation{
    private byte m_code;

    public BaseNotice(byte code) throws UnknownCodeException
    {
        if (!NoticeCodes.noticeName.containsKey(code))
            throw new UnknownCodeException();

        m_code = code;
    }

    public byte getCode() {
        return m_code;
    }

    public String getName() {
        return NoticeCodes.noticeName.get(m_code);
    }
}
