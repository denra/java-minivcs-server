package transfer.notices.base;

import transfer._bases.BaseFileOperation;
import transfer._exceptions.UnknownCodeException;

/**
 * Created by denra on 08.06.17.
 */
public class FileNotice extends BaseFileOperation{
    public FileNotice(byte code, int fileCount) throws UnknownCodeException {
        super(code, NoticeCodes.noticeName, fileCount);
    }
}
