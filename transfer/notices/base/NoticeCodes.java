package transfer.notices.base;

import java.util.HashMap;

/**
 * Created by denra on 08.06.17.
 */
public class NoticeCodes
{
    public static final byte ACCEPTED = 0;

    public static final String[] noticeName = new String[1];

    static {
        noticeName[ACCEPTED] = "accepted";
    }
}
