package transfer.notices.base;

import transfer._bases.BaseOperation;
import transfer._exceptions.UnknownCodeException;

/**
 * Created by denra on 08.06.17.
 */
public class Notice extends BaseOperation {
    public Notice(byte code) throws UnknownCodeException {
        super(code, NoticeCodes.noticeName);
    }
}
