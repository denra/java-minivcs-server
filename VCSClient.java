import java.io.*;
import java.net.Socket;

public class VCSClient {
    public static void makeQuery(byte[] query, String address) throws IOException
    {
        System.out.println("Welcome to Client side");
        Socket fromserver;

        System.out.println("Connecting to... " + address);

        fromserver = new Socket(address,10010);
        System.out.println("Connected ");

        BufferedReader reader  = new
                BufferedReader(new
                InputStreamReader(fromserver.getInputStream()));

        OutputStream writer = fromserver.getOutputStream();
        writer.write(query);

        reader.close();
        writer.close();
        fromserver.close();
    }
}
