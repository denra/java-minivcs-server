public interface ISerializable {
    public byte[] serialize();
    public ISerializable deserialize(byte[] bytes);
}
